Source: libyahc-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Andrius Merkys <merkys@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: libev-perl <!nocheck>,
                     libio-socket-ssl-perl <!nocheck>,
                     libjson-perl <!nocheck>,
                     libplack-perl <!nocheck>,
                     libtest-exception-perl <!nocheck>,
                     libtest-memory-cycle-perl <!nocheck>,
                     perl,
                     starman <!nocheck>
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libyahc-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libyahc-perl.git
Homepage: https://metacpan.org/release/YAHC

Package: libyahc-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libev-perl,
         libio-socket-ssl-perl
Description: Yet another HTTP client
 YAHC is fast & minimal low-level asynchronous HTTP client intended to be used
 where you control both the client and the server. Is especially suits cases
 where set of requests need to be executed against group of machines.
 .
 It is NOT a general HTTP user agent, it doesn't support redirects, proxies
 and any number of other advanced HTTP features like (in roughly descending
 order of feature completeness) LWP::UserAgent, WWW::Curl, HTTP::Tiny,
 HTTP::Lite or Furl. YAHC is basically one step above manually talking HTTP
 over sockets.
 .
 YAHC supports SSL and socket reuse (latter is in experimental mode).
